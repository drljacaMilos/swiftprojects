//
//  DataPreparation.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/16/21.
//

import Foundation

class DataPreparation{
    
    //Collection Views
    static var listOfMoviesInTrending: SearchMovies?
    static var listOfWhatToWatch: SearchMovies?
    static var listOfTopPicksForYou: SearchMovies?
    static var listOfFanFavourites: SearchMovies?
    static var listOfExploreWhatsStreaming: SearchMovies?
    static var listOfMoviesAndTvShows: SearchMovies?
    static var listOfComingSoon: SearchMovies?
    static var listOfWatchSoonAtHome: SearchMovies?
    static var listOfTopBoxOffice: [MovieBoxOffice]?
    
    //Url's
    static let sliderUrl = "http://www.omdbapi.com/?apikey=e709e37&s=dune"
    static let whatToWatchUrl = "http://www.omdbapi.com/?apikey=e709e37&s=none"
    static let topPicksForYouUrl = "http://www.omdbapi.com/?apikey=e709e37&s=took"
    static let fanFavouritesUrl = "http://www.omdbapi.com/?apikey=e709e37&s=mast"
    static let exploreWhatsStreamingUrl = "http://www.omdbapi.com/?apikey=e709e37&s=fight"
    static let exploreMoviesAndTvShowsUrl = "http://www.omdbapi.com/?apikey=e709e37&s=ball"
    static let comingSoonUrl = "http://www.omdbapi.com/?apikey=e709e37&y=2021&s=dark"
    static let whatToWatchAtHomeUrl = "http://www.omdbapi.com/?apikey=e709e37&type=series&s=white"
    
    
    
    //Fetching Data
    static func fetchMovie(url: String, completion: @escaping (SearchMovies?) -> Void){
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            
            if let error = error {
                print("Error accesing IMDB Api: \(error)")
            }
            
            guard let httpResponse = response as? HTTPURLResponse,
                  (200...299).contains(httpResponse.statusCode) else {
                print("Error with response, uninspected status code: \(response)")
                return
            }
            
            if let data = data, case let movieInfo = try? JSONDecoder().decode(SearchMovies.self, from: data){
                completion(movieInfo)
            }
        }
        task.resume()
    }
    
    //Fetching movies for box office
    static func fetchMovieBoxOffice(url: String, completion: @escaping (MovieBoxOffice?) -> Void){
        guard let apiURL = URL(string: url) else { return }
        
        let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
            if let data = data, case let movieInfo = try? JSONDecoder().decode(MovieBoxOffice.self, from: data){
                completion(movieInfo)
            }
        }
        task.resume()
    }
    
    
}
