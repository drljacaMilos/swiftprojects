//
//  CheckedBoxButtons.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/29/21.
//

import UIKit


class CheckedBoxButtons: UIButton {
    
    override func awakeFromNib() {
        
        self.setImage(UIImage(named:"icons8-checked-checkbox-24"), for: .selected)
        self.setImage(UIImage(named:"icons8-unchecked-checkbox-24"), for: .normal)
        
        self.addTarget(self, action: #selector(CheckedBoxButtons.buttonClicked(_:)), for: .touchUpInside)
        
//        iconImage = iconImage.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
    }
    

    @objc func buttonClicked(_ sender: UIButton) {
        self.isSelected = !self.isSelected
    }
}

