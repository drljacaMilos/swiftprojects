//
//  CellTopBoxOfficeTV.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/22/21.
//

import UIKit

class CellTopBoxOfficeTV: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBOutlet var movieNumber: UILabel!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var movieBoxOffice: UILabel!
}
