//
//  CellForMovieSliderCV.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/16/21.
//

import UIKit

class CellForMovieSliderCV: UICollectionViewCell {
    
    @IBOutlet var movieImage: UIImageView!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var shortDescription: UILabel!
    @IBOutlet var smallMovieImage: UIImageView!
}
