//
//  FoodExperienceAndEventProgramVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 15.7.21..
//

import UIKit

class FoodExperienceAndEventProgramVC: AudiEventiVC, UITableViewDelegate, UITableViewDataSource {
   

    override func viewDidLoad() {
        super.viewDidLoad()

       prepareNavigationBar()
        
    }
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageLabel: UIImageView!
    @IBOutlet var subtitleLabel: UILabel!
    
    @IBOutlet var meetingsTableView: UITableView!
    
    
    
//    var list = TestData.foodExperience?[0].programExperience
    var list = TestData.foodExperience
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("List count \(list?.count)")
        return list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ConfigData.foodExeperienceCell, for: indexPath) as! CellForFoodExperienceTV
        
//        cell.textLabel?.text = "Bla bla"

        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return list?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Celija"
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let headerView: UIView = UIView()
//        headerView.backgroundColor = UIColor.yellow
      
        let calendarButton = UIButton()

        calendarButton.setTitle("Venerdi 15 july", for: .normal)
        
        let phoneWidth = UIScreen.main.bounds.width
        
        calendarButton.setImage(UIImage(systemName: "chevron.down"), for: .normal)
        calendarButton.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        calendarButton.titleLabel!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        calendarButton.imageView!.transform = CGAffineTransform(scaleX: -1.0, y: 1.0)
        calendarButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: phoneWidth - 100)
        
//        calendarButton.setImage(UIImage(systemName: "calendar"), for: .normal)
        headerView.addSubview(calendarButton)
        calendarButton.translatesAutoresizingMaskIntoConstraints = false
        calendarButton.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        calendarButton.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        calendarButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        calendarButton.widthAnchor.constraint(equalToConstant: UIScreen.main.bounds.width).isActive = true
        
        calendarButton.setTitleColor(.black, for: .normal)
        calendarButton.tintColor = .black
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row selected \(indexPath.row)")
    }

    
}
