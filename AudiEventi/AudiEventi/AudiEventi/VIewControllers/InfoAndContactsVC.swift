//
//  InfoAndContactsVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 14.7.21..
//

import UIKit

class InfoAndContactsVC: AudiEventiVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBar()

        setInfoAndContactsData()
    }
    
    
    @IBOutlet var labelTitle: UILabel!
    @IBOutlet var infoAndContactsImage: UIImageView!
    @IBOutlet var infoAndContactsTitle: UILabel!
    
    
    func setInfoAndContactsData() {
        
        let infoAndContact = TestData.contactAndInfo
        
        labelTitle.text = infoAndContact?.title
        infoAndContactsTitle.text = infoAndContact?.title
        if let urlString = infoAndContact?.contactImage.href, let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){ (data, response, error) in
                if let data = data {
                    DispatchQueue.main.async {
                        self.infoAndContactsImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
    }
    
}
