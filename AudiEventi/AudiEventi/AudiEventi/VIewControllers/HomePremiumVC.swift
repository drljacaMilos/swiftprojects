//
//  HomePremiumVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 12.7.21..
//

import UIKit

class HomePremiumVC: AudiEventiVC {

    
    @IBAction func showEventList(_ sender: Any) {
        showViewController(identifier: ConfigData.eventiListViewController, positionOfDualViewControllerToShow: 0)
    }
    
    @IBAction func insertCoupon(_ sender: Any) {
        showAlertForCoupon()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBar()
    }

    @IBAction func showAudiDrivingExperience(_ sender: Any) {
        showViewController(identifier: ConfigData.placesAndAudiExperieanceViewController, positionOfDualViewControllerToShow: 1)
    }
    
    @IBAction func showPlacesAndTeritory(_ sender: Any) {
        showViewController(identifier: ConfigData.placesAndAudiExperieanceViewController, positionOfDualViewControllerToShow: 2)
    }
}
