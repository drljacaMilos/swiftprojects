//
//  NotificheVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 9.7.21..
//

import UIKit

class NotificheVC: AudiEventiVC, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var notificheTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBar()
    }
    
    //Table view count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TestData.notifications?.count ?? 0
    }
    
    //Table view cell for row at
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: ConfigData.notificheCell, for: indexPath) as! CellForNotificheTV
        
        let notification = TestData.notifications?[indexPath.row]
        
        if let notification = notification {
            let dateTime = notification.sentTime.components(separatedBy: "T")
            
            cell.dateLabel.text = dateTime[0].replacingOccurrences(of: "-", with: "/")
            
            print(dateTime[1].components(separatedBy: "+")[0])
            
            cell.hoursLabel.text = String(dateTime[1].components(separatedBy: "+")[0].prefix(5))
            
            cell.titleLabel.text = notification.title
            cell.descriptionLabel.text = notification.body
            
            Helper.makeLabelRadius(radius: 15, label: cell.hoursLabel)
            Helper.makeBorderToLabel(borderWitdh: 1, borderColor: UIColor.lightGray.cgColor, label: cell.hoursLabel)
        }

        return cell
    }

    //Setting header for table
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ConfigData.storticoNotifiche
    }
    
    //Setting headers background color
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
            (view as! UITableViewHeaderFooterView).contentView.backgroundColor = UIColor.white
        }
}
