//
//  EventiListVC.swift
//  AudiEventi
//
//  Created by milos.drljaca on 7.7.21..
//

import UIKit

class EventiListVC: AudiEventiVC, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet var eventiListTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBar()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TestData.publicEvents?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell =  tableView.dequeueReusableCell(withIdentifier: ConfigData.eventiListCell, for: indexPath) as! CellForEventiListTV
        
        let eventi = TestData.publicEvents?[indexPath.row]
        
        cell.eventiTitle.text = eventi?.title
        cell.eventiDescription.text = eventi?.description.value
        
        if let urlString = eventi?.backgroundImage?.href, let url = URL(string: urlString){
            URLSession.shared.dataTask(with: url){ (data,response,error) in
                if let data = data{
                    DispatchQueue.main.async {
                        cell.eventiImage.image = UIImage(data: data)
                    }
                }
            }.resume()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return CGFloat(380)
    }

}
