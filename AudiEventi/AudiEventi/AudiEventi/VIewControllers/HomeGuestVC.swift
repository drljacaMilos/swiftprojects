//
//  ViewController.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import UIKit

class HomeGuestVC: AudiEventiVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareNavigationBar()
    }
    
    
    @IBAction func showEventiList(_ sender: Any) {
        showViewController(identifier: ConfigData.eventiListViewController, positionOfDualViewControllerToShow: 0)
    }
    
    @IBAction func showCoupon(_ sender: Any) {
        showAlertForCoupon()
    }
}

