//
//  ResultProtocol.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import Foundation


protocol ResultProtocol: Codable {
    var result: String { get set}
    var resultCode: String { get set }
    var resultMessage: String { get set }
}


protocol CouponProtocol: Codable {
    var coupon: String { get set }
    var eventId: String { get set }
    var status: String { get set }
    var valid: String { get set }
}
