//
//  CouponModel.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import Foundation

struct CouponVerification: Codable, CouponProtocol, ResultProtocol{
    var coupon: String
    var eventId: String
    var status: String
    var valid: String
    var result: String
    var resultCode: String
    var resultMessage: String
}

struct CouponValidation: Codable, CouponProtocol, ResultProtocol{
    var coupon: String
    var eventId: String
    var status: String
    var valid: String
    var result: String
    var resultCode: String
    var resultMessage: String
    var action: String
}

