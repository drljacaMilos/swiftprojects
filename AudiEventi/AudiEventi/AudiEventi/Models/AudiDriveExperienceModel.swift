//
//  AudiDriveExperienceModel.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import Foundation


struct DriveExperienceResponse: Codable, ResultProtocol {
    var data: AudiDriveExperience
    var result: String
    var resultCode: String
    var resultMessage: String
}


struct AudiDriveExperience: Codable{
    var id: String
    var description: String
    var subtitleAde: String
    var titleAde: String
    var sliderImage: [BackgroundImage]
    var imageAde: BackgroundImage
}
