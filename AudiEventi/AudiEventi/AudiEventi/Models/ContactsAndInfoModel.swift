//
//  ContactsAndInfoModel.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import Foundation

struct ContactsAndInfo: Codable, ResultProtocol {
    var data: ContactsAndInfoData
    var result: String
    var resultCode: String
    var resultMessage: String
    
    //Add enum keys for all structures
}
struct ContactsAndInfoData: Codable {
    var id: String
    var title: String
    var contactImage: BackgroundImage
}
