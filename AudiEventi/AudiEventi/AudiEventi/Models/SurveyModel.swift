//
//  SurveyModel.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import Foundation


struct SurveyResponse: Codable, ResultProtocol {
    var questions: [Question]
    var status: String //enum
    var result: String //enum
    var resultCode: String //enum
    var resultMessage: String //enum

    //Add enum keys for all structures
}

struct Question: Codable {
    var webformId: String
    var title: String
    var questionId: String
    var required: String
    var requiredError: String
    var webformMultiple: String
    var type: String //  type of demand ?? radio buttons, text area, button?
    
}


struct SurveyPost: Codable{
    var options: [String: String]
    var questions: String
    var other: String
    var answers: String
}

