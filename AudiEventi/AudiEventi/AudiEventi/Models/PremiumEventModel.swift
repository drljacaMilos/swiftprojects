//
//  PremiumEventModel.swift
//  AudiEventi
//
//  Created by milos.drljaca on 1.7.21..
//

import Foundation


struct PremiumEvent: Codable, ResultProtocol{
    var data: [PremiumEventData]
    var result: String
    var resultCode: String
    var resultMessage: String
    
    
    //Add enum keys for all structures
}

struct PremiumEventData: Codable{
    var id: String
    var title: String
    var description: PremiumDescription
    var headerPremium: String
    var linkMyAudiPremium: LinkMyAudi
    var noteProgram: NoteProgram
    //Program details?
    var programDetails: [ProgramDetail]
    var subtitle: String
    var image: BackgroundImage?
}

struct PremiumDescription: Codable {
    var value: String
    var format: String
    var processed: String
}

struct NoteProgram: Codable {
    var value: String
    var format: String
    var processed: String // HTML tags?
}

struct ProgramDetail: Codable {
    
}

//programDetails:[Day]

struct Day: Codable{
    var day:String
    var activities:[Activity]
}

struct Activity: Codable{
    var start : String
    var end :String
    var activity: String
}
