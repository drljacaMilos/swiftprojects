//
//  TestData.swift
//  AudiEventi
//
//  Created by milos.drljaca on 7.7.21..
//

import Foundation

class TestData {
    
    //Data
    static var publicEvents: [PublicEvent]?
    static var premiumEvents: [PremiumEventData]?
    static var contactAndInfo: ContactsAndInfoData?
    static var surveyModelResponse: [Question]?
    static var foodExperience: [FoodExperienceData]?
    static var placesAndTerritory: PlacesAndTerritory?
    static var audiDriveExperience: AudiDriveExperience?
    static var notifications: [Notifications]?

    //Getting data from Json
    static func populateTestData(){
        
        publicEvents = []
        premiumEvents = []
        surveyModelResponse = []
        foodExperience = []
        notifications = []
        
        let decoder = JSONDecoder()
        
        
        //1.Public Event
        let publicEventData = publicEventiJSONData.data(using: .utf8)
        
        if let data = publicEventData, let result = try? decoder.decode([PublicEvent].self, from: data) {
            publicEvents = result
        }
        
        //2.Premium Event
        let premiumEventData = premiumEventJSONData.data(using: .utf8)
        
        if let data = premiumEventData, let result = try? decoder.decode(PremiumEvent.self, from: data) {
            premiumEvents = result.data
        }
        
        //3.Contact and Info
        let contactAndInfoData = contactAndInfoJSONData.data(using: .utf8)
        
        if let data = contactAndInfoData, let result = try? decoder.decode(ContactsAndInfo.self, from: data) {
            contactAndInfo = result.data
        }
        
        //4.Survey Response
        let surveyResponseData = surveyResponseJSONData.data(using: .utf8)
        
        if let data = surveyResponseData, let result = try? decoder.decode(SurveyResponse.self, from: data) {
            surveyModelResponse = result.questions
        }
        
        //5.Food Experience
        let foodExperienceData = foodExperienceJSONData.data(using: .utf8)
        
        if let data = foodExperienceData, let result = try? decoder.decode(FoodExperience.self, from: data) {
            foodExperience = result.data
        }
        
        //6.Places and Territory
        let placesAndTerritoryData = placesAndTerritoryJSONData.data(using: .utf8)
        
        if let data = placesAndTerritoryData, let result = try? decoder.decode(PlacesAndTerritoryResponse.self, from: data) {
            placesAndTerritory = result.data
        }
        
        //7.Audi drive experience
        let audiDriveExperienceData = audiDriveExperienceJSONData.data(using: .utf8)
        
        if let data = audiDriveExperienceData, let result = try? decoder.decode(DriveExperienceResponse.self, from: data) {
            audiDriveExperience = result.data
        }
        
        //8.Notifications
        let notificationsData = notificationJSONData.data(using: .utf8)
        
        if let data = notificationsData, let result = try? decoder.decode([Notifications].self, from: data) {
            notifications = result
        }
    }
    
}

let notificationJSONData = """
    [
        {
            "status": "Sent",
            "statusCode": "sent",
            "sentTime": "2021-01-29T16:31:07+0100",
            "title":  "Sent by confi",
            "topic":  "3470200e-b987-469c-a8e7-0d4ed4e02a56",
            "deepLink":  "/generic",
            "body":  "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc."
        },
        {
            "status": "Sent",
            "statusCode": "sent",
            "sentTime": "2021-01-29T15:54:02+0100",
            "title":  "Last Save Not Yet Saved",
            "topic":  "3470200e-b987-469c-a8e7-0d4ed4e02a56",
            "deepLink":  "/survey",
            "body":  "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
        }
    ]
    """

let audiDriveExperienceJSONData = """
    {
        "data": {
            "id": "1",
    
            "description": "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of de Finibus Bonorum et Malorum (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, Lorem ipsum dolor sit amet.., comes from a line in section 1.10.32.",
    
            "subtitleAde": "Al contrario di quanto si pensi, Lorem Ipsum ",
    
            "titleAde": "Audi driving Experience",
    
            "sliderImage": [{
                    "id": "1",
                    "href": "https://autoblog.rs/gallery/108/201217-audi%20a3%2033.jpg",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }, {
                    "id": "2",
                    "href": "https://c4.wallpaperflare.com/wallpaper/675/310/349/audi-rs6-audi-rs6-avant-quattro-wallpaper-preview.jpg",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }, {
                    "id": "3",
                    "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }, {
                    "id": "4",
                    "href": "https://audiclubserbia.com/club/wp-content/uploads/2021/02/1-audi-e-tron-s-sportback-2021-uk-first-drive-review-hero-front-450x290@2x.jpg",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }
    
            ],
    
            "imageAde": {
                "id": "2",
                "href": "https://topspeed.rs/gallery/thumb/foto-audi-1/page/1/marker/30868/photo/167060.jpeg",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }
    
    
        },
        "result": "200",
        "resultCode": "200",
        "resultMessage": "OK"
    
    
    }
    """


let placesAndTerritoryJSONData = """
    {
        "data": {
            "id": "1",
            "title": "Uno Due Tre Divergenti",
            "description": "Suspendisse auctor at nulla a ultrices. Suspendisse accumsan diam vitae eleifend blandit. Praesent a augue faucibus, feugiat diam ac, semper elit. Suspendisse et semper quam, quis euismod tortor. Integer congue ut risus id interdum. Fusce dapibus metus nec augue porta, blandit scelerisque sem sagittis. Maecenas interdum turpis vitae tortor euismod efficitur vitae sed massa.",
            "placeSubtitle": "Cognos",
            "placeTitle": "Nepal",
            "placesSlider": [{
                    "id": "1",
                    "href": "https://lh3.googleusercontent.com/proxy/EadLiyXSmp-ImpSDA0JhjluFl3o4JKopO-KycBlER42FEN1SBVbm7A3v7c5DGY070h3U-Ixge4hU_lHogGeyMDgYayWKdUBNa46a3YmyqCo",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }, {
                    "id": "2",
                    "href": "https://c4.wallpaperflare.com/wallpaper/485/216/1006/rain-cars-audi-drive-season-suv-1920x1200-nature-seasons-hd-art-wallpaper-preview.jpg",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }, {
                    "id": "3",
                    "href": "https://wallpapercave.com/wp/wp1830889.jpg",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }, {
                    "id": "4",
                    "href": "https://www.wallpaperbetter.com/wallpaper/337/959/740/audi-a4-audi-autumn-gold-1080P-wallpaper-middle-size.jpg",
                    "meta": {
                        "alt": "loading..",
                        "title": "Meta",
                        "width": 200,
                        "height": 300
                    }
                }],
            "imagePlace": {
                "id": "1",
                "href": "https://c.wallhere.com/photos/65/82/autumn_fall_nature_car_fog_landscape_highway_foggy-850144.jpg!d",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }
        },
        "result": "200",
        "resultCode": "200",
        "resultMessage": "OK"
    }
    """


let foodExperienceJSONData = """
     {
         "data": [{
             "id": "1",
             "title": "TITLE1",
             "header": "Jjaiojai najo",
             "foodSubtitle": {
                 "value": "String",
                 "format": "",
                 "processed": ""
             },
             "foodImage": {
                 "id": "1",
                 "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                 "meta": {
                     "alt": "loading..",
                     "title": "Meta",
                     "width": 200,
                     "height": 300
                 }
             },
             "programExperience": [
                 {
                     "day": "11.01.2021.",
                     "start": "08:00",
                     "type": "",
                     "activity": "brekfast",
                     "site": "",
                     "description": "",
                     "food": "",
                     "alergens": ""
                 },
                 {
                     "day": "21.02.2021.",
                     "start": "",
                     "type": "",
                     "activity": "",
                     "site": "",
                     "description": "",
                     "food": "",
                     "alergens": ""
                 }
             ]
         }],
         "result": "200",
         "resultCode": "200",
         "resultMessage": "OK"
     }
    """

let surveyResponseJSONData = """
        {
              "questions": [{
                    "webformId": "1",
                    "title": "Random title",
                    "questionId": "1",
                    "required": "Required",
                    "requiredError": "Required Error",
                    "webformMultiple": "Web Form",
                    "type": "Type"
                        }],
              "status": "Accepted",
              "result": "200",
              "resultCode": "200",
              "resultMessage": "OK"
    
        }
    """

let contactAndInfoJSONData = """
        {
              "data": {
                    "id": "1",
                    "title": "Info & Contatti",
                    "contactImage":{
                      "id": "1",
                      "href": "https://audimediacenter-a.akamaihd.net/system/production/media/101356/images/bd935377fbf8e708f3b1de51f448b3b741dad0a9/A213131_overfull.jpg?1621927712",
                      "meta": {
                          "alt": "loading..",
                          "title": "Meta",
                          "width": 200,
                          "height": 300
                                }
                            }
                        },
              "result": "200",
              "resultCode": "200",
              "resultMessage": "OK"

        }
"""

let premiumEventJSONData = """
       {
              "data": [{
                  "id": "1",
                  "title": "Uomini fiesta del Audi",
                  "description": {
                      "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                      "format": "nn",
                      "processed": "Y"
                  },
                  "headerPremium": "Bene bene",
                  "linkMyAudiPremium": {
                      "uri": "www.audi.de",
                      "title": "Audi",
                      "options": []
                  },
                  "noteProgram": {
                      "value": "Culinigo umelana",
                      "format": "Som",
                      "processed": "Uno"
                  },
                  "programDetails": [{
                      "day": "1",
                      "activities": [{
                          "start": "16:30",
                          "end": "20:30",
                          "activity": "Survey disponibile"
                      }]
                  }],
                  "subtitle": "Madona di Camiglio",
                  "image": {
                      "id": "1",
                      "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                      "meta": {
                          "alt": "loading..",
                          "title": "Meta",
                          "width": 200,
                          "height": 300
                      }
                  }
              }],
              "result": "200",
              "resultCode": "200",
              "resultMessage": "OK"
          }
        
    
    """

let publicEventiJSONData = """
[{
    "id": "1",
    "title": "Audi Talks Performance: ieri, oggi, e domani.",
    "description": {
        "format": "",
        "value": "Otto puntate con ospiti d'eccezione per condividare visioni all'avangurdia e discutere di una nouva, straordinaria idea"

    },
    "dataEvent": "02.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 4,
    "status": "OK",
    "backgroundImage": {
        "id": "1",
        "href": "https://trumpwallpapers.com/wp-content/uploads/Audi-Wallpaper-11-3840x2160-1-scaled.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
}, {
    "id": "2",
    "title": "Cortina e-portrait",
    "description": {
        "format": "",
        "value": "Dal 2017 Audi - per tutilare Cortina come patrimonio si e fatta promotrice con il Comune di Ampezzo di un progetto di Corporate..."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 1,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://r1.ilikewallpaper.net/iphone-4s-wallpapers/download/5614/audi-r8-front-iphone-4s-wallpaper-ilikewallpaper_com.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
},{
    "id": "3",
    "title": "Mountaineering Workshop con Herve ...",
    "description": {
        "format": "",
        "value": "Un evento dedicato alla montagna e al guisto approccio per viverla con rispetto e inteligenza, dalla attivita sportive come lo sci ..."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 2,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://bioneers.org/wp-content/uploads/2019/07/shutterstock_1157498146.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
},{
    "id": "4",
    "title": "Performance Workshop con Kristian Ghedina",
    "description": {
        "format": "",
        "value": "I pluricampioni Kristian Ghedina e Peter Fill, insieme agli chef stellati Costardi Bros, ti guideranno in un 'esperienza totale, tra sport."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 3,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://vindisgroup.com/storage/app/media/service/audi/audi_service_head.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
}

]
"""
