//
//  Config.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import Foundation


public struct ConfigData {
    
    //Identifier for Cells
    static let burgerMenuCell = "cellForBurgerMenu"
    static let eventiListCell = "cellForEventiList"
    static let notificheCell = "cellForNotifiche"
    static let foodExeperienceCell = "cellForFoodExperience"
    static let eventProgramCell = "cellForEventProgram"
    
    //View Controllers
    static let burgerMenuViewController = "BurgerVC"
    static let notificheViewController = "NotificheVC"
    static let homeGuestViewController = "HomeGuestVC"
    static let eventiListViewController = "EventiListVC"
    static let settingsViewController = "SettingsVC"
    static let homePremiumViewController = "HomePremiumVC"
    static let infoAndContactsViewController = "InfoAndContactsVC"
    //Dual View Controller
    static let placesAndAudiExperieanceViewController = "PlacesAndAudiExperienceVC"
    static let foodExperienceAndEventProgramViewController = "FoodExperienceAndEventProgramVC"
    
    
    //Segue
    static let homeGuestSegue = "homeGuest"
    static let homePremiumSegue = "homePremium"
    
    
    //Badges
    static let notificationBadge = "notification_badge"
    static let notificationNoBadge = "notification_no_badge"
    static let storticoNotifiche = "Storico Notifiche"
    
    //Menu items
    enum menuItems: String {
        case SIDE_MENU_GUEST_COUPON_LOGIN
        case SIDE_MENU_GUEST_EVENT_LIST
        case SIDE_MENU_GUEST_HOME_PAGE
        case SIDE_MENU_GUEST_SETTINGS
        case SIDE_MENU_PREMIUM_ADE
        case SIDE_MENU_PREMIUM_EVENT_DETAIL
        case SIDE_MENU_PREMIUM_EVENT_INFO
        case SIDE_MENU_PREMIUM_FOOD_EXPERIENCE
        case SIDE_MENU_PREMIUM_LOCATION
        case SIDE_MENU_PREMIUM_SURVEY
    }
}
