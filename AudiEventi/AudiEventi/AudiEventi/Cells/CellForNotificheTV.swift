//
//  CellForNotificheTV.swift
//  AudiEventi
//
//  Created by milos.drljaca on 9.7.21..
//

import UIKit

class CellForNotificheTV: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var hoursLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
}
