//
//  CellForMenuTV.swift
//  AudiEventi
//
//  Created by milos.drljaca on 2.7.21..
//

import UIKit

class CellForMenuTV: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBOutlet var buttonBurgerMenu: ButtonInMenu!
}
