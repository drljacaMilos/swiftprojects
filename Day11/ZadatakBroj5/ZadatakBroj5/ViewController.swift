//
//  ViewController.swift
//  ZadatakBroj5
//
//  Created by milos.drljaca on 5/12/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      
        facebook.layer.cornerRadius = 20
        twitter.layer.cornerRadius = 20
        
        login.layer.cornerRadius = 20
    }


    @IBOutlet weak var facebook: UIButton!
    @IBOutlet weak var twitter: UIButton!
    
    @IBOutlet weak var login: UIButton!
    
    
}

