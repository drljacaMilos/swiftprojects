//
//  ViewController.swift
//  Zadatak
//
//  Created by milos.drljaca on 5/14/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        fieldConstraints(stackViewNumberOne)
        fieldConstraints(stackViewNumberTwo)
        fieldConstraints(stackViewNumberThree)
        
        
        fieldConstraints(stackViewNumberFour)
        fieldConstraints(stackViewNumberFive)
        fieldConstraints(stackViewNumberSix)
        fieldConstraints(stackViewNumberSeven)

    }

    @IBOutlet weak var viewNumberOne: UIView!
    @IBOutlet weak var viewNumberTwo: UIView!
    @IBOutlet weak var viewNumberThree: UIView!
    
    
   
    
    @IBOutlet weak var stackViewNumberOne: UIStackView!
    
    @IBOutlet weak var stackViewNumberTwo: UIStackView!
    
    @IBOutlet weak var stackViewNumberThree: UIStackView!
    
    @IBOutlet weak var stackViewNumberFour: UIStackView!
    
    @IBOutlet weak var stackViewNumberFive: UIStackView!
    
    @IBOutlet weak var stackViewNumberSix: UIStackView!
    
    @IBOutlet weak var stackViewNumberSeven: UIStackView!
    
    
    
    func fieldConstraints (_ view : UIStackView! ) {
        view.layer.borderWidth = 5
        view.layer.cornerRadius = 20
        view.layer.borderColor = UIColor.cyan.cgColor
    }
}

