//
//  ViewController.swift
//  IMDbProject
//
//  Created by milos.drljaca on 6/15/21.
//

import UIKit

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var sliderCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        DataPreparation.pripareDataForSlider()
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        sleep(1)
        return DataPreparation.listOfMoviesInTrending!.Search.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = sliderCollectionView.dequeueReusableCell(withReuseIdentifier: "cellForMovieSlider", for: indexPath) as! CellForMovieSliderCV

        
        if let urlString =  DataPreparation.listOfMoviesInTrending!.Search[indexPath.row].poster as? String,
           let url = URL(string: urlString) {
               URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                   if let data = data {

                    DispatchQueue.main.async {
                        cell.movieImage.image = UIImage(data: data)
                        cell.smallMovieImage.image = UIImage(data: data)
                    }
                   }
               }.resume()
           }

        cell.movieName.text = DataPreparation.listOfMoviesInTrending!.Search[indexPath.row].title
        cell.shortDescription.text = DataPreparation.listOfMoviesInTrending!.Search[indexPath.row].type
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("You selected cell #\(indexPath.item)")
//        collectionView.
        collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
    }

}

