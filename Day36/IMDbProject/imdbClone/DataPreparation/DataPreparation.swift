//
//  DataPreparation.swift
//  imdbClone
//
//  Created by milos.drljaca on 6/16/21.
//

import Foundation

class DataPreparation{
    
    static var listOfMoviesInTrending: SearchMovies?
    static let baseUrl = URL(string: "http://www.omdbapi.com/?apikey=e709e37&s=star")!

    static func pripareDataForSlider(){
        fetchMovie{(movie) in
            guard let movie = movie else { return }
//            print("Fetched info")
            listOfMoviesInTrending = movie
           print(listOfMoviesInTrending!)
        }
    }
    
    static func fetchMovie(completion: @escaping (SearchMovies?) -> Void){

        let task = URLSession.shared.dataTask(with: baseUrl){ (data, response, error) in
            if let data = data, let movieInfo = try? JSONDecoder().decode(SearchMovies.self, from: data){
                
                completion(movieInfo)
//                print("RESULT MOVIE INFO: \(movieInfo)")
            }
    }
        task.resume()
    }

}
