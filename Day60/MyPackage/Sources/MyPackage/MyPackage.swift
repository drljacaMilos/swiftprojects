

import Foundation


public struct MyPackage {
    var text = "Hello, World!"
    
    public static func printSomeSpecialText(){
        print("This is the info from package.")
    }
}
